from constants import *
from parameters import *
from functions import *
from classes import *

number_of_points = 100

model = Model(parameters, units, 'bgi', number_of_points)

model.Pulsars[0].graph_psi_old()
"""
graph_fun(J0, 0, 10, 'p', 'J0', 'bessel')
"""