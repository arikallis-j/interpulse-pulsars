"""
Математические/Физические/Графические функции
"""
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy.integrate import odeint
from scipy.special import jv
from constants import *


"""Математические функции"""
def rad(x):
	return x*pi/180

def deg(x):
	return x*180/pi

def cos(x):
	return np.cos(x)

def sin(x):
	return np.sin(x)

def ln(x):
	return np.log(x)

def log(x):
	return np.log(x)/np.log(10)

def exp(x):
	return np.exp(x)
	
def exp10(x):
	return 10**x

def J0(x):
	return jv(0,x)

def hs(x):
	return np.heaviside(x,1)

def minimum(x,y):
	return np.minimum(x,y)

def arc(x,y):
	if ((x>0) and (y>=0)):
		phi = np.arctan(abs(y/x))
		return phi
	if ((x<0) and (y>=0)):
		phi = np.arctan(abs(y/x))
		return pi - phi
	if ((x<0) and (y<=0)):
		phi = np.arctan(abs(y/x))
		return pi + phi
	if ((x>0) and (y<=0)):
		phi = np.arctan(abs(y/x))
		return 2*pi - phi
	if (x==0) and (y>=0):
		return pi/2
	if (x==0) and (y<=0):
		return pi + pi/2

def z_div(x,y):
	if y==0:
		return inf
	else:
		return x/y

def polar(x,y):
	r = np.sqrt(x*x + y*y)
	phi = arc(x,y)
	return r, phi

def decart(r, phi):
	x = r*np.cos(phi)
	y = r*np.sin(phi)
	return x, y

def Sigma(x, i, n):
	summ = 0
	for k in range(i,n+1):
		summ += x(k)
	return summ

def el(l,k):
	return l[k]

def fon(x):
	if x>1:
		return 1
	else:
		return None

def eq_gamma(y, x, a, b0, b1, c0, c1, d):
	if d < 1:
		dydx = - a*y**4 + (b0 - c0*x)*hs(b0 - c0*x)
	else:
		dydx = - a*y**4 + b1*exp(-c1*x)
	return dydx

def eq_delta(y, x, a, b0, b1, c0, c1, d):
	if d < 1:
		dydx = (b0 - c0*x)*hs(b0 - c0*x)
	else:
		dydx = b1*exp(-c1*x)
	return dydx



"""Физические функции"""

#####################
#Pulsar_set_functions
#####################
def Omega(P):
	"""
	Частота вращения #s^-1
		P - период вращения #s
	"""
	return 2*pi/P

def W50_angle(P, W50_time):
	"""
	Ширина профиля на 50% пика, угол #rad
		P - период вращения #s
		W50_time - ширина профиля на 50% пика, время #s
	"""
	return 2*pi*(W50_time/P)

def sigma(beta, W50_angle):
	"""
	Угол сдвига линии наблюдателя #rad
		beta - угол между осью магнитного поля и наблюдателем? #rad
		W50_angle - ширина профиля на 50% пика, угол #rad
	"""
	return np.arctan(beta/W50_angle)

def B_bgi(P, P_dot):
	"""
	Магнитное поле модели BGI #G
		P - период #s
		P_dot - производная периода #1
	"""
	const = exp10(12) * epsilon**(-1/2)
	p = P/exp10(0)
	p_dot = P_dot/exp10(-15)
	return const * p**(3/4) * p_dot**(1/2)

def B_mhd(P,P_dot):
	"""
	Магнитное поле модели BGI #G
		P - период #s
		P_dot - производная периода #1
	"""
	const = exp10(12) * 1.04/2**0.5  
	p = P/exp10(0)
	p_dot = P_dot/exp10(-15)
	return const * p**(1/2) * p_dot**(1/2)

def B_mid(P,P_dot):
	"""
	Магнитное поле модели BGI #G
		P - период #s
		P_dot - производная периода #1
	"""
	return B_0

def B_all(P, P_dot):
	"""
	Магнитное поле в зависимости от модели # dict['model']
		P - период #s
		P_dot - производная периода #1
	"""
	b_bgi = B_bgi(P, P_dot)
	b_mhd = B_mhd(P, P_dot)
	b_mid = B_mid(P, P_dot)
	b_all = {'bgi': b_bgi, 'mhd': b_mhd, 'mid': b_mid}
	return b_all

#######################
#Pulsar_model_functions
#######################
def R_0(P, model = 'mid'):
	"""
	Радиус полярной шапки #cm
		P - период вращения #s^-1
		model - используемая модель(bgi/mhd/test) #str
	"""
	omega = Omega(P)
	return f_s[model]**(1/2) * (omega*R/c)**(1/2) * R

####################
#Point_set_functions
####################
def spherical(r, phi, R_0):
	"""
	Функция сферических координат на полярной шапке #(cm,rad,rad)
		r - радиус на полярной шапке #1
		phi - угол на полярной шапке #deg
		R_0 - радиус полярной шапки #cm
	"""
	r_m = r * R_0
	phi_m = rad(phi)
	theta_m = r_m / R
	return r_m, phi_m, theta_m

def R_c(r_m):
	"""
	Радиус кривизны #cm
		r_m - радиус в полярных координатах шапки #cm
	"""
	return z_div(4/3 * R**2, r_m)

def theta_b(chi, r_m, phi_m):
	"""
	Угол между магнитным полем и осью вращения #rad
		chi - угол между осью вращения и магнитной осью #rad
		r_m - радиус на полярной шапке #cm
		phi_m - угол на полярной шапке #rad , 
	"""
	return chi - 3/2 * (r_m/R) * sin(phi_m)

def H_RS(P, B, R_c, theta_b):
	"""
	Высота Рудермана-Сезерленда #cm
		P - период #s
		B - магнитное поле #G
		R_c - радиус кривизны #cm
		theta_b - угол между магнитным полем и осью вращения #rad
	"""
	const = 1.1 * exp10(4)
	cos_t = abs(cos(theta_b))
	p = P/exp10(0)
	r_c = R_c/exp10(7)
	b = B/exp10(12) 
	return const * cos_t**(-3/7) * p**(3/7) * r_c**(2/7) * b**(-4/7)

def pho_GJ(Omega, B, theta_b):
	"""
	Гольдрайховская плотность #q_sgs/cm^3
		Omega - частота вращения #s^-1
		B - магнитное поле #G
		theta_b - угол между магнитным полем и осью вращения #rad
	"""
	return Omega*B*cos(theta_b)/(2*pi*c)

def psi_RS(H_RS, pho_GJ):
	"""
	Потенциал Рудермана-Сезерленда #G*cm
		H_RS - высота Рудермана-Сезерленда #cm
		pho_GJ - гольдрайховская плотность #q_sgs/cm^3
	"""
	psi_rs = 2*pi*pho_GJ*H_RS**2
	###Потенциал Рудермана-Сезерленда не может быть отрицательным!
	###Поэтому при psi_rs<0 : psi_rs := 0;
	###Или домножим на функцию Хевисайда от psi_rs.
	return psi_rs * hs(psi_rs)

def psi_VC(Omega, B, R_0, chi, r_m, phi_m, theta_m):
	"""
	Вакуумный потенциал #G*cm
		Omega - частота вращения #s^-1
		B - магнитное поле #G
		R_0 - радиус полярной шапки #cm
		chi - угол между осью вращения и магнитной осью #rad
		r_m, phi_m, theta_m - сферические координаты на полярной шапке #cm, rad
	"""
	theta_0 = R_0/R
	const = 1/2 * (Omega*B*R_0**2)/c * (1 - (theta_m/theta_0)**2)
	return const * (cos(chi) + 3/4*theta_m*sin(phi_m)*sin(chi))

def psi(psi_RS, psi_VC):
	"""
	Общий потенциал (в центре и по краям не выходит за вакуумный) #G*cm
		psi_RS - потенциал Рудермана-Сезерленда #G*cm
		psi_VC - вакуумный потенциал #G*cm
	"""
	return minimum(psi_RS,psi_VC)

def psi_old(Omega, B, R_0, chi, r_m, H_RS):
	"""
	Ещё не подкрученный ваукуумный потенциал #G*cm
	"""
	const = 1/2 * (Omega*B*R_0**2)/c * cos(chi)
	f = lambda k: c_i[k]*exp(-lambda_i[k]*H_RS/R_0) * J0(lambda_i[k]*r_m/R_0)
	return const * (1 - (r_m/R_0)**2 - Sigma(f, 0, 2))


def psi_new(Omega, B, R_0, chi, r_m, H_RS):
	"""
	Подкрученный ваукуумный потенциал #G*cm
	"""
	const = 1/2 * (Omega*B*R_0**2)/c * cos(chi)
	a_i = lambda k: c_i[k] / (1 + exp(-2*lambda_i[k]*H_RS/R_0))
	b_i = lambda k: c_i[k] / (1 + exp(+2*lambda_i[k]*H_RS/R_0))
	f = lambda k: (a_i(k)*exp(-lambda_i[k]*H_RS/R_0) + b_i(k)*exp(+lambda_i[k]*H_RS/R_0)) * J0(lambda_i[k]*r_m/R_0)
	return const * (1 - (r_m/R_0)**2 - Sigma(f, 0, 0))

def get_dif_coeff(R_0, R_c, H_RS, psi_RS, psi_VC):
	"""
	Функция, выдающая коэффициенты дифференциального уравнения #turple<sgs>
		R_0 - радиус полярной шапки #cm
		R_c - радиус кривизны #cm
		H_RS - высота Рудермана-Сезерленда #cm
		psi_RS - потенциал Рудермана-Сезерленда #G*cm
		psi_VC - вакуумный потенциал #G*cm
	"""
	A_coeff = 2/3 * e_mc2 * (e*R_0)/(R_c**2)
	B0_coeff = 2 * psi_RS * e_mc2 * (R_0/H_RS)
	B1_coeff = psi_VC * e_mc2 * lambda_i[0] 
	C0_coeff = 2 * psi_RS * e_mc2 * (R_0/H_RS)**2
	C1_coeff = lambda_i[0]
	D_coeff = z_div(psi_RS,psi_VC)

	return (A_coeff, B0_coeff, B1_coeff, C0_coeff, C1_coeff, D_coeff)

#########################
#Point_equation_functions
#########################

def epsilon_e(dif_args,R_0):
	"""
	Энергия частицы #erg
		dif_args - коэффициенты уравнения на фактор gamma #turple<sgs>
		R_0 - радиус полярной шапки #cm
	"""
	x = np.linspace(0, R/R_0, exp10(h_int))
	gamma = odeint(eq_gamma, gamma0, x, args=(dif_args))
	delta = odeint(eq_delta, gamma0, x, args=(dif_args))
	gamma1, delta1 = gamma[-1][0], delta[-1][0]

	return (delta1 - gamma1)*mc2

def Lambda_r(Lambda_0):
	"""
	Относительный логарифмический фактор #1
		Lambda_0 - абсолютный логарифмический фактор
	"""
	return Lambda_0 - 3*ln(Lambda_0)

def Lambda_0(e_ph, B, R_c):
	"""
	Aбсолютный логарифмический фактор через энергию фотона #1
		e_ph - энергия фотона #erg
		B - магнитное поле #G
		R_c - радиус кривизны #cm
	"""
	return ln(e**2/(h_pl) * (omega_b0*R_c/c) * (B_cr/B)**2  * (mc2/e_ph)**2)

def Epsilon_ph(l0, B, R_c):
	"""
	Энергия фотона через абсолютный логарифмический фактор #erg
		lo - aбсолютный логарифмический фактор #1
		B - магнитное поле #G
		R_c - радиус кривизны #cm
	"""
	return 8/(3*Lambda_r(l0)) * (mc2) * (B_cr/B) * (R_c/R)

def epsilon_ph(B, R_c):
	"""
	Энергия фотона, реккурентная форма #erg
		B - магнитное поле #G
		R_c - радиус кривизны #cm
	"""
	l0 = Lambda_0_mid
	for k in range(h_int):
		eph = Epsilon_ph(l0, B, R_c)
		l0 = Lambda_0(eph, B, R_c)
	return eph

def lambda_point(epsilon_e, epsilon_ph, r):
	"""
	Отношение концентрации частиц к гольдреховской #1
		epsilon_e - энергия частицы #erg
		epsilon_ph - энергия фотона #erg
	"""
	return (epsilon_e/epsilon_ph) * hs(1-r)


"""Графические функции"""

def graph_fun(f, a, b, key = 'p', sh_name = 'y(x)', wh_name = 'fun'):
	"""
	Функция отрисовки графика функции
		f - функция
		a - предел слева
		b - предел справа
	"""
	X = np.linspace(a, b, N_pt)
	Y = f(X)
	print(Y)
	plt.style.use('seaborn-whitegrid')
	fig, ax = plt.subplots()

	ax.set_xlim(a,b)
	ax.plot(X, Y, 'b-', linewidth=2, label='$'+sh_name+'$')


	ax.set_title("Functuon " + wh_name, fontsize=20)
	ax.set_xlabel('$x$')
	ax.set_ylabel('$f(x)$')
	ax.legend()

	if key=='s':
		if not os.path.isdir('fun'):
			os.mkdir('fun')
		plt.savefig('fun/'+ 'function' + '_' + sh_name + '_' + wh_name + '.png')
	elif key=='p':
		plt.show()
	else: 
		return 0


#######################
#Pulsar_graph_functions
#######################

def picture_lambda(pulsar, x_arr, y_arr, z_arr, N, xf_arr, yf_arr, zf_arr, key = 'p', line = False):
	"""
	Функция отрисовки множественности lambda на полярной шапке
		pulsar - пульсар #class<Pulsar>
		x_arr - значения координат точек пульсара по оси X
		y_arr - значения координат точек пульсара по оси Y
		z_arr - значения lambda для точек (x,y)
		N - количество точек пульсара по каждой оси
		xf_arr - значения координат фоновых точек по оси X
		yf_arr - значения координат фоновых точек по оси Y
		zf_arr - значения фона для точек (x,y) [None, 0]
		key - ключ формата представления #str ['p': print, 's': save]
		line - ключ отрисовки линии наблюдения #bool
	"""
	plt.style.use('_classic_test_patch')
	fig, ax = plt.subplots()

	norm = mpl.colors.Normalize(round(np.min(z_arr)),round(np.max(z_arr)))

	fig.colorbar(mpl.cm.ScalarMappable(norm = norm))
	ax.contourf(x_arr, y_arr, z_arr, N_lev, norm = norm)
	ax.contourf(xf_arr, yf_arr, zf_arr, N_lev, norm = norm, colors = ['w'])
	if line:
		x = np.linspace(-cos(pulsar.sigma),cos(pulsar.sigma), N)
		y = +sin(pulsar.sigma)+(x*0)
		ax.plot(x, y, 'b-', linewidth=2, label='$\gamma_{0}$')

	fig.set_figwidth(9)  #ширина и
	fig.set_figheight(8) #высота "Figure"

	ax.set_title("Pulsar " + pulsar.name, fontsize=20)
	ax.set_xlabel('$X$')
	ax.set_ylabel('$Y$')

	if key=='s':
		if not os.path.isdir('pic'):
			os.mkdir('pic')
		plt.savefig('pic/'+ pulsar.name + '_' + pulsar.model + '.png')
	elif key=='p':
		plt.show()
	else:
		print("dfdd")
		return 0

def graph_profile(pulsar, x, z, zs, key = 'p'):
	"""
	Функция отрисовки профиля излучения по линии наблюдения
		pulsar - пульсар #class<Pulsar>
		x - множество точек по оси X
		z - значения интенсивности на MainPulse
		zs - значения интенсивности на InterPulse
		key - ключ формата представления #str ['p': print, 's': save]
	"""
	plt.style.use('seaborn-whitegrid')
	fig, ax = plt.subplots()

	ax.set_xlim(-1,1)
	ax.plot(x, z, 'b-', linewidth=2, label='$profile-Main$')
	ax.set_title("Pulsar " + pulsar.name, fontsize=20)
	ax.plot(x, zs, 'r-', linewidth=2, label='$profile-Inter$')

	ax.set_xlabel('$\phi, deg$')
	ax.set_ylabel('$\lambda$')
	ax.legend()

	if not os.path.isdir('prof'):
		os.mkdir('prof')
	plt.savefig('prof/'+ pulsar.name + '_' + pulsar.model + '_' + 'MI' + '.png')

	if key=='s':
		if not os.path.isdir('prof'):
			os.mkdir('prof')
		plt.savefig('prof/'+ pulsar.name + '_' + pulsar.model + '_' + 'MI' + '.png')
	elif key=='p':
		plt.show()
	else:
		return 0

def graph_psi(pulsar, X, Z, V, R, key = 'p'):
	"""
	Функция отрисовки потенциала psi по линии y = kx + b
		pulsar - пульсар #class<Pulsar>
		X - множество точек по оси X
		Z - значения суммарного потенциала 
		V - значения вакуумного потенциала
		R - значения потенциала Рудермана-Сезерленда
		k - коэффициент наклона прямой #1 [-inf, +inf]
		b - коэффициент сдвига прямой #1 [-1, 1]
		key - ключ формата представления #str ['p': print, 's': save]
	"""
	plt.style.use('seaborn-whitegrid')
	fig, ax = plt.subplots()

	ax.set_xlim(-1,1)
	ax.plot(X, V, 'g-', linewidth=2, label='$\psi_{VC}$')
	ax.plot(X, R, 'r-', linewidth=2, label='$\psi_{RS}$')
	ax.plot(X, Z, 'b-', linewidth=2, label='$\psi$')

	ax.set_title("Pulsar " + pulsar.name, fontsize=20)
	ax.set_xlabel('$r/R_{0}$')
	ax.set_ylabel('$\psi$')
	ax.legend()

	if key=='s':
		if not os.path.isdir('psi'):
			os.mkdir('psi')
		plt.savefig('psi/'+ 'psi_' + pulsar.name + '_' + pulsar.model + '.png')
	elif key=='p':
		plt.show()
	else: 
		return 0
		
######################
#Point_graph_functions
######################

def graph_gamma(point, x, gamma, delta, key = 'p'):
	"""
	Функция отрисовки зависимости фактора gamma от расстояния
		point - точка на пульсаре #class<Point>
		gamma - массив точек фактора gamma с потерями
		delta - массив точек фактора gamma без потерь
		x - массив точек с относительным расстоянии
		key - ключ формата представления #str ['p': print, 's': save]
	"""
	plt.style.use('seaborn-whitegrid')
	fig, ax = plt.subplots()

	ax.set_xlim(0,x1)
	ax.plot(x, delta, 'r-', linewidth=2, label='$\gamma_{0}$')
	ax.plot(x, gamma, 'b-', linewidth=2, label='$\gamma_{e}$')

	ax.set_title('$r_{m}$' + "=" + str(round(point.r,3)) + ', $\phi_{m}$' + "="+ str(round(point.phi)) + '°', loc='center', fontsize=20)
	ax.set_xlabel('$h/R_{0}$')
	ax.set_ylabel('$\gamma$')
	ax.legend()

	if key=='s':
		if not os.path.isdir('graph'):
			os.mkdir('graph')
		plt.savefig('graph/'+ point.pulsar.name + '_' + point.pulsar.model + '_' + str(round(point.r,3)) + '+' + str(round(point.phi)) + '.png')
	elif key=='p':
		plt.show()
	else: 
		return 0