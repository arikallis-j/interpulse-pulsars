"""
Классы

Pulsar - пульсар
Point - точка на пульсаре
Model - общая модель
"""

from constants import *
from parameters import *
from functions import *

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy.integrate import odeint
import os


class Pulsar():
	"""
	Класс Пульсар
	*Переменные:
		name - имя в каталоге #str
		chi - угол между магнитной осью и осью вращения #rad
		beta - угол между осью магнитного поля и наблюдателем? #rad
		P - период пульсара #s
		P_dot - производная периода #1
		Omega - частота вращения #s^-1
		W50 - ширина профиля на 50% пика, время #s
		sigma - угол сдвига линии наблюдателя #rad
		points - массив точек #list<Point>
		B_bgi - магнитное поле модели BGI #G
		B_mhd - магнитное поле модели MHD #G
		B_mid - среднее магнитное поле #G
		B_all - магнитное поле в зависимости от модели #dict['model']
		model - используемая модель #key
		R_0 - радиус полярной шапки #cm
		B - магнитное поле, с учетом модели #G
	*Методы:
		set_parameters - присвоение параметров из базы данных (!)
		set_model - установка модели (!)
		set_point - добавление новой точки на полярной шапке
		picture_lamda - функция, отображающая множественность lambda на полярной шапке
		graph_profile - функция, отображающая теоретический профиль излучения
		graph_psi - функция, отображающая потенциалы в разрезе прямой kx+b
	"""
	def set_parameters(self, name, chi, beta, P, P_dot, W50):
		"""
		Инициализация пульсара #void
			name - имя в каталоге #str
			chi - угол между магнитной осью и осью вращения #rad
			beta - угол между осью магнитного поля и наблюдателем? #rad
			P - период пульсара #s
			P_dot - производная периода #1
			W50 - ширина профиля на 50% пика, время #s
		"""
		self.name = name
		self.chi = min(chi, pi-chi)
		self.beta = abs(beta)
		self.P = P
		self.P_dot = P_dot
		self.Omega = Omega(self.P)

		self.W50 = W50_angle(P, W50)
		self.sigma = sigma(self.beta, self.W50)
		
		self.points = []

		self.B_bgi = B_bgi(self.P, self.P_dot)
		self.B_mhd = B_mhd(self.P, self.P_dot)
		self.B_mid = B_mid(self.P, self.P_dot)
		self.B_all = B_all(self.P, self.P_dot)

	def set_model(self, model):
		"""
		Установка модели #void
			model - используемая модель #key
		"""
		self.model = model
		self.R_0 = R_0(self.P, self.model)
		self.B = self.B_all[self.model]

	def set_point(self, r, phi):
		"""
		Добавление точки с полярными координатами #void
			r - радиус на полярной шапке #1 [0,1]
			phi - угол на полярной шапке #deg [0,360)
		"""
		x, y = decart(r, rad(phi))
		self.points.append(Point(self, x, y))

	def picture_lambda(self, key = 'p', line = False):
		"""	
		Множественность lamda на полярной шапке #picture
			key - ключ формата представления #str ['p': print, 's': save]
			line - ключ отрисовки линии наблюдения #bool
		"""
		X_arr, Y_arr, N = [], [], int((len(self.points))**0.5)

		for k in range(N):
			X_arr.append(self.points[k*N].x)
			Y_arr.append(self.points[k].y)

		x_arr, y_arr = np.meshgrid(X_arr, Y_arr)
		z_arr = x_arr*0
		for i in range(N):
			for j in range(N):
				k = i*N + j
				self.points[k].lambda_p()
				z_arr[j][i] = self.points[k].lamda
				print("points: ", k, "/", N*N)

		X_arr = np.linspace(-1, 1, N_fon)
		Y_arr = np.linspace(-1, 1, N_fon)
		xf_arr, yf_arr = np.meshgrid(X_arr, Y_arr)
		zf_arr = xf_arr*0
		for i in range(N_fon):
			for j in range(N_fon):
				k = i*N_fon + j
				x, y = xf_arr[j][i], yf_arr[j][i]
				r, phi = polar(x,y)
				zf_arr[j][i] = fon(r)
				print("fon: ", k, "/", N_fon*N_fon)
		picture_lambda(self, x_arr, y_arr, z_arr, N, xf_arr, yf_arr, zf_arr, key, line)

	def graph_profile(self, key = 'p'):
		"""
		Профиль излучения по линии наблюдения #void
			key - ключ формата представления #str ['p': print, 's': save]
		"""
		x = np.linspace(-cos(self.sigma),cos(self.sigma), N_pt) / cos(self.sigma)
		y = sin(self.sigma) + (x*0)
		z = (x*0)
		for k in range(N_pt):
			point = Point(self, x[k], y[k])
			point.lambda_p()
			z[k] = point.lamda

		ys = -sin(self.sigma) + (x*0)
		zs = (x*0)
		for k in range(N_pt):
			point = Point(self, x[k], ys[k])
			point.lambda_p()
			zs[k] = point.lamda

		#отрисовка
		graph_profile(self, x, z, zs, key)

	def graph_psi(self, key = 'p', k = 0, b = 0):
		"""
		Потенциал Рудермана-Сазерленда, вакуумный потенциал и сшивка, срез по линии y = kx + b #void
			k - коэффициент наклона прямой #1 [-inf, +inf]
			b - коэффициент сдвига прямой #1 [-1, 1]
			key - ключ формата представления #str ['p': print, 's': save]
		"""
		X = np.linspace(-1, 1, N_pt)
		Y = (X*k) + b
		Z, V, R = (X*0), (X*0), (X*0)
		for k in range(N_pt):
			point = Point(self, X[k], Y[k])
			Z[k] = point.psi
			V[k] = point.psi_VC
			R[k] = point.psi_RS
		#отрисовка
		graph_psi(self, X, Z, V, R, key = key)

	def graph_psi_old(self, key = 'p', k = 0, b = 0):
		"""
		Потенциал вакуумный неподкрученный (синий), подкрученный (зелёный) срез по линии y = kx + b #void
			k - коэффициент наклона прямой #1 [-inf, +inf]
			b - коэффициент сдвига прямой #1 [-1, 1]
			key - ключ формата представления #str ['p': print, 's': save]
		"""
		X = np.linspace(-1, 1, N_pt)
		Y = (X*k) + b
		Z, V, R = (X*0), (X*0), (X*0)
		for k in range(N_pt):
			point = Point(self, X[k], Y[k])
			Z[k] = point.psi_old
			V[k] = point.psi_new
			#R[k] = point.psi_RS
		#отрисовка
		graph_psi(self, X, Z, V, R, key = key)


class Point():
	"""
	Класс Точка на пульсаре
	*Переменные:
		pulsar - пульсар, которому принадлежит точка #class<Pulsar>
		lamda - значение множественности в этой точки #1
		r, phi - координаты на полярной шапке, красивые #(1,deg)
		r_m, phi_m, theta_m - сферические координаты на полярной шапке #(cm, rad, rad)
		R_c - радиус кривизны #cm
		theta_b - угол между магнитным полем и осью вращения #rad
		H_RS - высота Рудермана-Сезерленда #cm
		pho_GJ - гольдрайховская плотность #q_sgs/cm^3
		psi_RS - потенциал Рудермана-Сезерленда #G*cm
		psi_VC - вакуумный потенциал #G*cm
		psi - сшивка двух потенциалов
		psi_old - неподкрученный потенциал
		psi_new - подкрученный потенциал (пока не работает)
		Xd_coeff - точка перегиба графика gamma, отношение Hrs/R0
		dif_args - коэффициенты дифференциального уравнения #turple<sgs>
	*Методы:
		__init__ - присвоение параметров точке
		graph_gamma - функция, отображающая зависимость gamma-фактора от расстояния (решение уравнения)
		epsilon_e - функция, вычисляющая значение энергии частицы, летевшей из этой точки
		epsilon_ph - функция, вычисляющая значение энергии фотона, летевшего из этой точки
		lambda_p - функция, вычисляющая значение множественности в этой точке
	"""
	def __init__(self, pulsar, x, y):
		"""
		Функция инициализации #void
			x, y - координаты на полярной шапке #(1,1) [-1,1]
		"""
		self.pulsar = pulsar
		self.lamda = 0

		r, phi = polar(x,y)
		self.x, self.y = x, y
		self.r, self.phi = r, deg(phi)
		self.r_m, self.phi_m, self.theta_m = spherical(self.r, self.phi, self.pulsar.R_0)

		self.R_c = R_c(self.r_m)
		self.theta_b = theta_b(self.pulsar.chi, self.r_m, self.phi_m)
		self.H_RS = H_RS(self.pulsar.P, self.pulsar.B, self.R_c, self.theta_b)
		self.rho_GJ = pho_GJ(self.pulsar.Omega, self.pulsar.B, self.theta_b)

		self.psi_RS = psi_RS(self.H_RS, self.rho_GJ)
		self.psi_VC = psi_VC(self.pulsar.Omega, self.pulsar.B, self.pulsar.R_0, self.pulsar.chi, self.r_m, self.phi_m, self.theta_m)
		self.psi = psi(self.psi_RS, self.psi_VC)
		self.psi_old = psi_old(self.pulsar.Omega, self.pulsar.B, self.pulsar.R_0, self.pulsar.chi, self.r_m, self.H_RS)
		self.psi_new = psi_new(self.pulsar.Omega, self.pulsar.B, self.pulsar.R_0, self.pulsar.chi, self.r_m, self.H_RS)

		self.Xd_coeff = self.H_RS/self.pulsar.R_0
		self.dif_args = get_dif_coeff(self.pulsar.R_0, self.R_c, self.H_RS, self.psi_RS, self.psi_VC)

	def graph_gamma(self, key = 'p'):
		"""
		Зависимость фактора gamma от расстояния #void
			key - ключ формата представления #str ['p': print, 's': save]
		"""
		#вычисления
		x = np.linspace(0, x1, N_pt)
		gamma = odeint(eq_gamma, gamma0, x, args=(self.dif_args))
		delta = odeint(eq_delta, gamma0, x, args=(self.dif_args))

		#отрисовка
		graph_gamma(self, x, gamma, delta, key)

	def epsilon_e(self):
		"""Энергия частицы #erg"""
		return epsilon_e(self.dif_args, self.pulsar.R_0)

	def epsilon_ph(self):
		"""Энергия фотона #erg"""
		return epsilon_ph(self.pulsar.B, self.R_c)

	def lambda_p(self):
		"""Множественность lambda #void"""
		self.lamda = lambda_point(self.epsilon_e(), self.epsilon_ph(), self.r)

class Model():
	"""
	Класс Пульсар
	*Переменные:
		parametes - данные по интеримпульсным пульсарам #list<dict['par']>
		units - праметры системах единиц в данных #dict['par']
		model_pulsar - используемая пульсарная модель #key
		n_points - количество точек n = n_points^2 на пульсаре 
		Pulsars - список пульсаров в модели #list<Pulsar>
	*Методы:
		graph_gamma - добавление новой точки на полярной шапке, пульсару номер n
		picture_lamda - функция, отображающая множественность lambda на полярной шапке, пульсару номер n
		graph_profile - функция, отображающая теоретический профиль излучения, пульсару номер n
		graph_psi - функция, отображающая потенциалы в разрезе прямой kx+b, пульсару номер n
	"""
	def __init__(self, parameters, units, model_pulsar, number_of_points):
		self.parameters = parameters
		self.units = units
		self.model_pulsar = model_pulsar

		self.n_points = number_of_points
		X = np.linspace(-1,1, self.n_points)
		Y = np.linspace(-1,1, self.n_points)

		self.Pulsars = []

		for n in range(len(self.parameters)):
			name = self.parameters[n]['name']
			chi = self.parameters[n]['chi'] * self.units['chi']
			beta = self.parameters[n]['beta'] * self.units['beta']
			P = self.parameters[n]['P'] * self.units['P']
			P_dot = self.parameters[n]['P_dot'] * self.units['P_dot']
			W50 = self.parameters[n]['W50'] * self.units['W50']
			pulsar = Pulsar()
			pulsar.set_parameters(name, chi, beta, P, P_dot, W50)
			pulsar.set_model(self.model_pulsar)

			for i in range(self.n_points):
				for j in range(self.n_points):
					r, phi = polar(X[i], Y[j])
					pulsar.set_point(r, deg(phi))

			self.Pulsars.append(pulsar)

	def graph_gamma(self, n, r, phi, key = 'p'):
		x, y = decart(r, rad(phi))
		point = Point(self.Pulsars[n], x, y)
		point.graph_gamma(key)

	def picture_lambda(self, n, key = 'p', line = False):
		self.Pulsars[n].picture_lambda(key, line)

	def graph_profiles(self, n, key = 'p'):
		self.Pulsars[n].graph_profile(key)

	def graph_psi(self, n, key = 'p', k = 0, b = 0):
		self.Pulsars[n].graph_psi(key, k, b)